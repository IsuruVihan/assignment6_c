/*
    Isuru Harischandra
    Student ID :- 202032

    * This program will calculate the fibonacci sequence from 0 to n (user input)
*/

# include <stdio.h>

// fibonacci calculator
int fibonacciSeq(int n){
    if (n==0) return 0;
    else if (n == 1) return 1;
    else return fibonacciSeq(n-1)+fibonacciSeq(n-2);
}

// start
int main() {
    int i, n;
    // getting user input
    printf("Enter an integer...\n");
    scanf("%d", &n);
    // calculating fibonacci sequence from 0 to n
    for (i = 0; i <= n; i++) {
        printf("%d\n", fibonacciSeq(i));
    }
    return 0;
}