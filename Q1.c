/*
    Isuru Harischandra
    Student ID : 202032 (Temporary)

    *This program will print an angled triangle pattern according to user input
*/

# include <stdio.h>

void pattern(int n);

// start
int main() {
    // row count of triangle, 'n'
    int n;

    // getting user input for 'n'
    puts("Enter an integer for 'n': ");
    scanf("%d", &n);
    pattern(n);

    return 0;
}

// print triangle
void pattern(int n) {
    if (n > 0) {
        pattern(n - 1);
        while (n > 0) {
            printf("%d ", n);
            n -= 1;
        }
        printf("\n");
    }
}